<?php
/* baraye didan error ha va debug kardan
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */

//load file haye zarori
$config = require_once('config.php');
require_once('query.php');
require_once('functions.php');
include('phpqrcode/qrlib.php');


db_start();

//etminan az ersale etelat tavasote karbar

if (
    !isset($_POST['author']) ||
    !isset($_POST['text_kind']) ||
    !isset($_POST['text']) ||
    !isset($_POST['ktime']) ||
    !isset($_POST['csrf_token']) ||
    empty($_POST['csrf_token']) ||
    empty($_POST['text_kind']) ||
    empty($_POST['text']) ||
    empty($_POST['ktime']) ||
    empty($_POST['author'])
    ) {
    exit("لطفا ابتدا فرم را پر کنید");
}

// chechk kardan csrf token
    session_start();
    if (!hash_equals($_SESSION['token'], $_POST['csrf_token'])) {
        exit("لطفا فرم را پر کنید.");
    }

//etminan az sehate etelaat

if (!in_array($_POST['text_kind'], $config['valid_text_types'])) {
    exit('لطفا نوع متن را درست انتحاب کنید');
};


//ba time_stamp va keep time E ke karbar dade keep_time database ro mohasebe mikonim

$keep_time = time() + $_POST['ktime'];
$keep_date = date("Y-m-d H:i:s", $keep_time);

//sabte data E ke karbar ferestade

db_submit($_POST['author'], $_POST['text_kind'], $_POST['text'], $keep_date, $_SERVER['REMOTE_ADDR']);

//generate kardane link ba estefade az id

$id = db_insert_id();
echo "<a href='{$config['homeurl']}/show.php?id={$id}'> click kon </a>";

//generate kardane QR code

//QRcode::png('PHP QR Code :)');
