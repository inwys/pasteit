<?php

/* baraye didan error ha va debug kardan
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
 */

//load file haye zarori
$config = require_once ('config.php');
require_once ('query.php');
require_once ('functions.php');
db_start();


//hamchin paste va id E vojod dare
if(!isset($_GET['id']) || !db_fetch_exists($_GET['id'])){
    exit("آدرس اشتباه وارد شده است");
}

$paste = db_fetch($_GET['id']);


//keeptime nagzashte bashe
$keep_time_ts_format = strtotime($paste['keep_time']);
if (time() > $keep_time_ts_format){
    exit ("مهلت نمایش متن به پایان رسیده است");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="/pasteit/highlights/styles/default.css">
    <script src="/pasteit/highlights/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
    <table>
        <tr><td>Author:</td><td><?php echo $paste['author']?></td></tr>
        <tr><td>text:</td><td><?php echo "<pre><code class =" .'"'. $paste['text_kind'].'">'. $paste['text']. "</code></pre>" ?></td></tr>
    </table>
</body>
</html>
