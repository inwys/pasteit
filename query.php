<?php
//motaghayeri ke azash baraye zakhire kardan etelate login e db estefade mishe
$con = Null;
//shoroe db, set kardan charset
function db_start()
{
    global $config;
    global $con;
    $db = $config['db'];
    if (!$con = mysqli_connect($db['host'], $db['user'], $db['pass'], $db['name'])) {
        exit('mysql error');
    }

    mysqli_query($con, 'SET NAMES \'utf8\'');
    mysqli_set_charset($con, 'utf8');

    return $con;
}
//query zadan be db

function db_query($query)
{
    global $con;
    return mysqli_query($con, $query);
}

//query be sorate array bargarde
function db_array_query($query)
{
    $result = db_query($query);
    $rows = [];
    if ($result && mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
    }
    return $rows;

}
//be dast avordane id

function db_insert_id()
{
    global $con;
    return mysqli_insert_id($con);
}

//sabte etelate paste to db
function db_submit($author, $text_kind, $text, $keep_time,$ip)
{
    $author = db_escape($author);
    $text = db_escape($text);
    $text_kind = db_escape($text_kind);
    $keep_time = db_escape($keep_time);
    $ip = db_escape($ip);
    db_query("INSERT INTO `pastes` (`author`, `text_kind`, `text`, `keep_time` , `ip`) 
    VALUES ($author, $text_kind, $text , $keep_time, $ip)");
}
//daryafte etelaeate paste az db

function db_fetch($id)
{
    $id = db_escape($id);
    return db_array_query("SELECT * FROM `pastes` where `id`= $id") [0];
}

//baraye check kardane sehate eteleat, bebinim un paste vojod dare ya na
function db_fetch_exists ($id)
{
    $id = db_escape($id);
    return db_array_query("SELECT COUNT(*) as total FROM `pastes` where `id`={$id}")[0]['total'];

}

//bastane db
function db_end()
{
    global $con;
    mysqli_close($con);
}

//jelogiri az sql injection

function db_escape($value)
{
    global $con;
    return is_null($value) ? 'null' : '\'' . mysqli_real_escape_string($con, $value) . '\'';
}