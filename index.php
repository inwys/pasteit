<?php
/*baraye debug kardan va didan error ha
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/


//load kardane file haye zarori

$config = require_once('config.php');
require_once('query.php');

//token generate mikonim baraye csrf
session_start();
if (empty($_SESSION['token'])) {
    $_SESSION['token'] = bin2hex(random_bytes(32));
}
$token = $_SESSION['token'];

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title> <?php echo $config['title']?></title>
</head>

<body>

<form action="result.php" method="post">
    <input type="hidden" name="csrf_token" value="<?php echo $token ?>">
    Author <input type="text" name="author">
    Title <input type="text" name="title">
    <select name="text_kind">
        <option value="apacheconf">Apache</option>
        <option value="c++">C++</option>
        <option value="coffee">Coffee</option>
        <option value="csharp">Csharp</option>
        <option value="css">Css</option>
        <option value="bash">Bash</option>
        <option value="diff">Diff</option>
        <option value="html">Html</option>
        <option value="http">HTTP</option>
        <option value="ini">ini</option>
        <option value="json">JSON</option>
        <option value="java">Java</option>
        <option value="javascript">JavaScript</option>
        <option value="makefile">Makefile</option>
        <option value="markdown">Markdown</option>
        <option value="nginx">Nginx</option>
        <option value="objectivec">Objective-C</option>
        <option value="perl">Perl</option>
        <option value="python">Python</option>
        <option value="ruby">Ruby</option>
        <option value="sql">SQL</option>
        <option value="shell">Shell</option>
        <option value="nohighlight" selected="selected">Text</option>



    </select>
    <select name="ktime">
        <option value="999999" selected="selected">Keep forever</option>
        <option value="300">5 minutes</option>
        <option value="3600">1 Hour</option>
        <option value="86400">1 Day</option>
        <option value="604800">1 Week</option>

    </select>
    <textarea name="text"></textarea>
    <input name="submit" type="submit" value="ثبت">


</form>

</body>

</html>